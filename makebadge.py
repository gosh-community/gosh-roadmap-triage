#! /usr/bin/env python3

import re
with open('triage_report.txt', 'r') as report:
    report_text = report.read()

updated = len(re.findall('_UPDATED_ITEM_', report_text))
new = len(re.findall('_CREATED_ITEM_', report_text))

with open('RoadmapBadge.svg', 'r') as rmb:
    svg = rmb.read()

svg = svg.replace('n99', str(new))
svg = svg.replace('e99', str(updated))
with open('badge.svg', 'w') as badge:
    badge.write(svg)
