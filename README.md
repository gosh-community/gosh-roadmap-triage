# Roadmap triage - This runs GOSH-BOT!

Once a month this repository sends [GOSH-BOT](https://gitlab.com/GOSH-BOT) off to hunt for stale issue (not updated for 3 months). GOSH-BOT replies to each of these issues asking for an update.

GOSH-BOT tries to be a good bot. If GOSH-BOT annoys you please let him know in the issues on the repository, or ask Julian on the [forum](https://forum.openhardware.science/).
